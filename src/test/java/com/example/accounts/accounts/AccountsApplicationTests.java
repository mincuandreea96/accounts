package com.example.accounts.accounts;

import static java.time.Duration.ofMillis;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTimeout;
import static org.junit.jupiter.api.Assertions.fail;

import java.net.URI;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.web.client.RestTemplate;

import com.example.accounts.models.Account;
import com.example.accounts.services.AccountService;
import com.example.accounts.servicesImpl.RateWS;

@SpringBootTest
class AccountsApplicationTests {

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	AccountService accountServiceImpl;

	@Autowired
	RateWS rateWS;

	private static final String notFoundAccount = "accountNotFound";
	private static final String foundAccount = "testIban";

	/*
	 * test method findAccountByIban from AccountServiceImpl class if return null
	 * that means the searched account doesn't exist - account not found
	 */
	@Test
	void testAccountNotFoundUsingAccountServiceClass() {
		assertNull(accountServiceImpl.findAccountByIban(notFoundAccount));
	}

	/*
	 * test rest controller AccountController class/ method to search account and
	 * convert balance to EUR
	 */
	@Test
	void testAccountNotFoundUsingAccountControllerClass() {
		URI uri = URI.create("http://localhost:8084/search-account/" + notFoundAccount);
		Map<String, Map<String, String>> resultMap = null;
		try {
			resultMap = this.restTemplate.getForObject(uri, HashMap.class);
			fail();

		} catch (Exception e) {
			// it must come here in order to validate the test
			assertNull(resultMap);
		}
	}

	/*
	 * the first call of the method to get rate will always get the rates from the
	 * ws and not from cache
	 */
	@Test
	@CacheEvict(value = "rates", allEntries = true)
	void testAccountFoundAndAUnCachedRates() {
		assertNotNull(accountServiceImpl.findAccountByIban(foundAccount));
		assertNotNull(rateWS.getEUExchangeRates());
	}

	/*
	 * performance test - when the rates are stored in cache the retrieval process
	 * takes less time
	 */
	@Test
	void testAccountFoundAndCachedRates() {
		assertNotNull(accountServiceImpl.findAccountByIban(foundAccount));
		assertTimeout(ofMillis(10), () -> {
			rateWS.getEUExchangeRates();
		});

	}

	@Test
	void testRightSetterCurrencyAccount() {

		Account account = new Account(2l, "iban", "RON", 3.22d, new Date());
		String expectedCurrency = "GBP";
		account.setCurrency(expectedCurrency);

		assertEquals(expectedCurrency, account.getCurrency());
	}

}
