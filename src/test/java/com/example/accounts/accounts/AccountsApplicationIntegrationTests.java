package com.example.accounts.accounts;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.accounts.models.Account;
import com.example.accounts.repositories.AccountRepository;
import com.example.accounts.servicesImpl.AccountServiceImpl;
import com.example.accounts.servicesImpl.RateWS;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class AccountsApplicationIntegrationTests {

	@Mock
	RateWS rateWS;

	@Autowired
	AccountRepository accountRepository;

	@Autowired
	@InjectMocks
	AccountServiceImpl accountServiceImpl;

	private static final String ibanWithNoMappedCurrencyInMockedRateWS = "iban3";
	private static final String ibanWithMappedCurrencyInMockedRateWS = "testIban";
	private static final String ibanNotFound = "ibanNotFound";

	/*
	 * the account is found in the database, but no mapping for account currency in
	 * mocked RateWS
	 */
	@Test
	public void testAccountFoundWithNoMapForCurrencyInMockWS() {

		Account account = accountServiceImpl.findAccountByIban(ibanWithNoMappedCurrencyInMockedRateWS);
		assertNotNull(account);

		when(rateWS.getEUExchangeRates()).thenReturn(createMockRatesValue());
		Double balanceBeforeExchange = account.getBalance();
		account = accountServiceImpl.exchangeToEuro(account);
		Double balanceAfterExchange = account.getBalance();

		assertEquals(balanceBeforeExchange, balanceAfterExchange);

	}

	/*
	 * test for account found and with a currency correspondent in the database
	 */
	@Test
	public void testAccountFoundWithCorrespondentCurrencyInMockedRateWS() {

		Account account = accountServiceImpl.findAccountByIban(ibanWithMappedCurrencyInMockedRateWS);
		assertNotNull(account);

		Double exchangeRate = (Double) createMockRatesValue().get(account.getCurrency());
		Double expectedBalanceValue = account.getBalance() / exchangeRate;
		when(rateWS.getEUExchangeRates()).thenReturn(createMockRatesValue());
		account = accountServiceImpl.exchangeToEuro(account);

		Double actualBalanceValue = account.getBalance();
		assertEquals(expectedBalanceValue, actualBalanceValue);
	}

	/*
	 * test for account not found
	 */
	@Test
	public void testAccountNotFound() {

		Account account = accountServiceImpl.findAccountByIban(ibanNotFound);
		assertNull(account);

	}

	public Map createMockRatesValue() {
		Map<String, Double> mapRates = new HashMap<String, Double>() {
			{
				put("CAD", 1.5273d);
				put("HKD", 8.4571d);
				put("ISK", 156.3d);
				put("PHP", 55.349d);
				put("CAN", 0.89563d);
				put("RON", 4.8435d);

			}
		};
		return mapRates;
	}
}
