package com.example.accounts.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.example.accounts.models.Account;
import com.example.accounts.services.AccountService;
import com.example.accounts.servicesImpl.RateWS;

@RestController
public class AccountsController {

	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(AccountsController.class);

	@Autowired
	AccountService accountServiceImpl;
	
	@Autowired
	RateWS rateWS;

	@GetMapping("/search-account/{iban}")
	public ResponseEntity<Account> searchAccount(@PathVariable String iban) {

		Account account = accountServiceImpl.findAccountByIban(iban);
		if (account == null) {
			return new ResponseEntity<Account>(HttpStatus.NOT_FOUND);
		}
	
		account = accountServiceImpl.exchangeToEuro(account);
		return new ResponseEntity<Account>(account, HttpStatus.OK);
	}

	@GetMapping("/getRates")
	public String getRates() {
		return rateWS.getEUExchangeRates().toString();

	}

}
