package com.example.accounts.servicesImpl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.accounts.models.Account;
import com.example.accounts.repositories.AccountRepository;
import com.example.accounts.services.AccountService;

@Service
public class AccountServiceImpl implements AccountService {

	@Autowired
	AccountRepository accountRepository;

	@Autowired
	RateWS rateWS;

	@Override
	public void saveAccount(Account account) {
		// TODO Auto-generated method stub
		accountRepository.save(account);
	}

	@Override
	public Account findAccountByIban(String Iban) {
		// TODO Auto-generated method stub
		return accountRepository.findByIban(Iban);

	}

	public Account exchangeToEuro(Account account) {
		Map<String, Double> mapExhangeValues = rateWS.getEUExchangeRates();
		Double rate = mapExhangeValues.get(account.getCurrency());
		if (rate != null) {
			account.setBalance(account.getBalance() / rate);
		}
		return account;
	}

}
