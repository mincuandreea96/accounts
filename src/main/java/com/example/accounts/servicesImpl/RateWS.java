package com.example.accounts.servicesImpl;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Service
public class RateWS {

	private final RestTemplate restTemplate;

	public RateWS(RestTemplate rest) {
		this.restTemplate = rest;
	}

	@HystrixCommand(fallbackMethod = "fallback_method")
	@Cacheable(value = "rates")
	public Map getEUExchangeRates() {
		// to simulate some wating time
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		URI uri = URI.create("https://api.exchangeratesapi.io/latest");
		Map<String, Map<String, String>> resultMap = this.restTemplate.getForObject(uri, HashMap.class);

		return resultMap.get("rates");
	}

	public Map fallback_method() {
		return new HashMap<String, String>();
	}
}
