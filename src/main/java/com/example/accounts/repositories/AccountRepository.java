package com.example.accounts.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.accounts.models.Account;

public interface AccountRepository extends JpaRepository<Account, Long> {

	@Query("select a from Account a  where a.iban like :iban")
	Account findByIban(String iban);

}
