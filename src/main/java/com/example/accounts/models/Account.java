package com.example.accounts.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "account")
@Entity
public class Account {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long idAccount;

	@Column(name = "iban")
	String iban;

	@Column(name = "currency")
	String currency;

	@Column(name = "balance")
	Double balance;

	@Column(name = "last_date_update")
	Date lastUpdateDate;

	public Account() {

	}

	public Account(Long idAccount, String iban, String currency, Double balance, Date lastUpdateDate) {
		super();
		this.idAccount = idAccount;
		this.iban = iban;
		this.currency = currency;
		this.balance = balance;
		this.lastUpdateDate = lastUpdateDate;
	}

	public Long getIdAccount() {
		return idAccount;
	}

	public void setIdAccount(Long idAccount) {
		this.idAccount = idAccount;
	}

	public String getIban() {
		return iban;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Double getBalance() {
		return balance;
	}

	public void setBalance(Double balance) {
		this.balance = balance;
	}

	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	@Override
	public String toString() {
		return "Account [idAccount=" + idAccount + ", iban=" + iban + ", currency=" + currency + ", balance=" + balance
				+ ", lastUpdateDate=" + lastUpdateDate + "]";
	}

}
