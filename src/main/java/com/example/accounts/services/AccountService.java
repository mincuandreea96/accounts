package com.example.accounts.services;

import com.example.accounts.models.Account;

public interface AccountService {

	void saveAccount(Account account);

	Account findAccountByIban(String Iban);

	Account exchangeToEuro(Account account);

}
