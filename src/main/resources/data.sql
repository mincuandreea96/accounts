DROP TABLE IF EXISTS account;

CREATE TABLE account (
  id_account  bigint(20) AUTO_INCREMENT  PRIMARY KEY,
  balance double DEFAULT NULL,
  currency varchar(255) DEFAULT NULL,
  iban varchar(255) DEFAULT NULL,
  last_date_update datetime DEFAULT NULL,
);

INSERT INTO account (balance, currency,iban,last_date_update) VALUES
  (1, 'CAN', 'testIban',null);
  
INSERT INTO account (balance, currency,iban,last_date_update) VALUES
  (2, 'RON', 'iban2',null);  
  
INSERT INTO account (balance, currency,iban,last_date_update) VALUES
  (3, 'GBP', 'iban3',null);